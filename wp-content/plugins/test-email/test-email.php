<?php
/**
 * Plugin Name:      Test Email
 * Plugin URI:        https://example.com/plugins/the-basics/
 * Description:       Handle the basics with this plugin.
 * Version:           1.10.3
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            ruban
 * Author URI:        https://author.example.com/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       test-email
 * Domain Path:       /languages
 *
 */

defined('ABSPATH') or die();
define( 'USERLIST__PLUGIN_URL', plugin_dir_url( __FILE__ ) );

require_once(__DIR__ . '/class-user-register.php');

class TestEmail {
    public function __construct() {
        add_action( 'admin_menu', array($this, 'add_admin_pages') );
        add_action( 'admin_init', array($this, 'eg_settings_api_init') );
        add_action( 'init', array($this,'set_email_message_filter') );
    }

    /**
     * fitler hooks pre update option
     */
    public function set_email_message_filter(){
        add_filter( 'pre_update_option_email_message', array($this, 'update_field_email_message'), 10, 2 );

    }
    public function update_field_email_message($newValue, $oldValue){
        $newValue = apply_filters( 'pre_update_message_filter',$newValue );
        return $newValue;
    }
    public function add_admin_pages(){
        add_menu_page( 'Test Email', 'Test Email', 'manage_options', 'test_email_menu', array($this,'cb_admin_index') );
        add_submenu_page( 'test_email_menu', 'Send Email', 'Send Email', 'manage_options', 'send_email_page', array($this,'cb_admin_index'));
    }

    public function eg_settings_api_init(){
        add_settings_section( 'eg_email_setting_section', __('Email Setting Section using Setting Api'), array($this, 'eg_setting_section_callback_function'),'send_email_page' );

        add_settings_field( 'email_subject', 'Email Subject', array($this, 'eg_email_subject_callback_function'), 'send_email_page', 'eg_email_setting_section' );
        add_settings_field( 'email_message', 'message', array($this, 'eg_email_message_callback_function'), 'send_email_page', 'eg_email_setting_section' );
        add_settings_field( 'email_from', 'Email From', array($this, 'eg_email_from_callback_function'), 'send_email_page', 'eg_email_setting_section' );


        register_setting( 'send_email_page', 'email_subject' );
        register_setting( 'send_email_page', 'email_message' );
        register_setting( 'send_email_page', 'email_from' );
    }

    public function eg_setting_section_callback_function() {
        _e('setting section','test-email');
    }

//add_settings_field( string $id, string $title, callable $callback, string $page, string $section = 'default', array $args = array() )
    public function eg_email_from_callback_function(){
        echo '<input name="email_from" id="email_from" type="text" value="'.get_option('email_from').'"/>';

    }
    public function eg_email_message_callback_function(){
        echo '<textarea name="email_message" id="email_message" >'.get_option('email_message').'</textarea>';

    }
    public function eg_email_subject_callback_function(){
        echo '<input name="email_subject" id="email_subject" type="text" value="'.get_option('email_subject').'"/>';

    }

    public function cb_admin_index(){
    ?>
            <form action="options.php" method="post">

                    <?php settings_errors();
                    do_settings_sections( 'send_email_page' );

                    // output security fields for the registered setting "send_email_page"
                    settings_fields( 'send_email_page' );

                    submit_button( 'Save Settings' );
                    ?>
            </form>
<?php
    }
}

$obj = new TestEmail();
