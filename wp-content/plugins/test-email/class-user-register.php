
<?php
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 9/22/2020
 * Time: 11:45 AM
 */
require_once(__DIR__ . '/class-notification.php');

class UserRegister{
    function __construct(){
        add_action( 'init',array($this,'initialize') );
        add_action( 'wp_ajax_my_add_user', array($this, 'my_add_user') );
        add_action( 'wp_enqueue_scripts', array($this, 'register_enqueue_scripts' ) );
    }

    public function register_enqueue_scripts(){
        wp_enqueue_style( 'bootstrap', '//stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css' );
        wp_enqueue_script( 'frontend-ajax', USERLIST__PLUGIN_URL.'custom.js', array('jquery'), false, true );
        wp_localize_script( 'frontend-ajax', 'frontend_ajax_object',
            array(
                'ajaxurl' => admin_url( 'admin-ajax.php' ),
                'lang_user_is_added' => __( 'User is added', 'test-email' ),
                'lang_not_logged_in' => __( 'You are not logged in', 'test-email' ),
                'lang_something_wrong_happened' => __( 'OOPs something wrong happened', 'test-email' )
            )
        );
    }

    public function initialize(){
        add_shortcode('user-register', array($this, 'add_user_form'));
    }

    public function my_add_user(){
        $this->validate_form();

        $email = sanitize_email( $_POST['email'] );
        $username = sanitize_user( $_POST['username'] );
        $first_name = sanitize_text_field( $_POST['first_name'] );
        $last_name = sanitize_text_field( $_POST['last_name'] );
        $password = $_POST['password'];
        $role = sanitize_text_field( $_POST['role'] );

        $data = array('user_email' => $email,
                      'user_login' => $username,
                        'user_pass' => $password,
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        'role' => $role
                        );

        $user_id = wp_insert_user( $data );
        $user = get_user_by( 'id', intval($user_id) );

        do_action( 'user_registered', $user->data );
    }

    private function validate_form(){
        $form_error = new WP_Error();

        if( empty($_POST['email']) ){
            $form_error->add( 'email', __( 'email is required', 'test-email') );
        }
        elseif ( ! is_email( $_POST['email'] ) ) {
            $form_error->add( 'email', __( 'Email is not valid', 'test-email') );
        }elseif(email_exists($_POST['email'])){
            $form_error->add( 'email', __( 'Email already exists', 'test-email') );
        }

        if(empty($_POST['username'])){
            $form_error->add( 'username', __(  'username is required', 'test-email' ));
        }elseif(username_exists($_POST['username'])){
            $form_error->add( 'username', __( 'username already exists', 'test-email' ) );
        }

        if(empty($_POST['password'])){
            $form_error->add( 'password', __( 'password is required', 'test-email' ));
        }

        if($form_error->has_errors()){
            wp_send_json_error( $form_error->errors, 422 );
//            status_header(422);
//            echo json_encode($form_error->errors); exit;
        }
    }
    public function add_user_form($attr){
        global $wp_roles;
        ?>
        <div class="alert alert-danger" style="color:red;display: none;" role="alert" id="error-block"></div>
        <div class="alert alert-success" style="color:green;display: none;" role="alert" id="success-block"></div>
        <form id="userForm" method="post">
            <div class="form-group">
                <label for="exampleInputEmail1"><?php _e( 'Username*', 'test-email' )?></label>
                <input type="text" class="form-control" id="username" name="username" aria-describedby="emailHelp"">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1"><?php _e( 'Email address*', 'test-email' )?></label>
                <input type="text" class="form-control" id="email" name="email" aria-describedby="emailHelp" >
            </div>

            <div class="form-group*">
                <label for="exampleInputEmail1"><?php _e( 'Password*', 'test-email' )?></label>
                <input type="password" class="form-control" id="password" name="password" aria-describedby="emailHelp"">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1"><?php _e( 'First Name', 'test-email' )?></label>
                <input type="text" class="form-control" id="first_name" name="first_name" aria-describedby="emailHelp" >
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1"><?php _e( 'Last Name', 'test-email' )?></label>
                <input type="text" class="form-control" id="last_name" name="last_name" aria-describedby="emailHelp" >
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1"><?php _e( 'Role', 'test-email' )?></label>
            <select id="role" class="form-control" name="role">
                <?php foreach( $wp_roles->roles as $k=>$role ) {?>
                    <option value="<?php esc_html_e($k) ?>"> <?=$k?> </option>
                <?php } ?>
            </select>
             </div>

            <input type="hidden" name="action" value="my_add_user">

            <input type="button" onclick="submitForm()" value="submit">
        </form>

    <?php
    }
}
$user = new UserRegister();




?>
