<?php

class Notification{
    public function __construct(){
        add_action( 'user_registered', array($this, 'user_register_notification'), 10, 1 );

    }
    public function user_register_notification($user){
        Notification::send( $user->user_email );
    }
    public static function send($to){
        $from = get_option('email_from');
        $message = get_option('email_message');
        $subject = get_option('email_subject');
        @mail($to,$subject,$message);
    }
}
new Notification();
