window.$ = jQuery;
function submitForm(){

    var url = frontend_ajax_object.ajaxurl;
    $.post(url, $('#userForm').serialize(),function(){
        $('#userForm').trigger('reset');
        show_success_msg(frontend_ajax_object.lang_user_is_added);

    },'json').fail(function(e) {
        if(e.status==400){
            alert(frontend_ajax_object.lang_not_logged_in);
        }
        else if(e.status==422){
            let error= e.responseJSON.data;
            show_error_msg(parseJsonError(error));
        }else{
            alert(frontend_ajax_object.lang_something_wrong_happened);
        }
    });
}
function show_success_msg(msg){
    $("#error-block").html('');
    $("#success-block").html(msg);
    $("#success-block").show();
    $('#error-block').hide();
}
function show_error_msg(msg){
    $("#error-block").html(msg);
    $("#success-block").hide();
    $("#error-block").show();
}
function parseJsonError(error){
    let msg = '<ul>';
    for(var k in error){
        msg = msg + '<li>'+error[k][0]+'</li>';
    }
    msg = msg+'</ul';
    return msg;
}
